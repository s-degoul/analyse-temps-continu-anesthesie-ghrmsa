# How to use this program?

## Set configuration

- rename `config.example.R` file to `config.R`
- check configuration parameters

## Set a directory tree for the analysed period

For one period of interest, create a directory named with start and end dates (same as in configuation file), format = `yyyy.mm.dd`, separated with a dash.

In this directory should stand two subdirectories, for source data and results of analyses (same name as in configuation file, default = `donnees` and `resultats` respectively).

Data directory contains raw schedule data and spreasheet containings manual adjustment of schedule (`Opendocument sheet` format).

## Launch analyses

- To create detailed HTML report: execute `render_display_computations.R`
- To create summary Excel file: execute `extract_computations_table.R`

> When running R scripts, working directory is supposed to be the project root directory

Results are produced in `resultats` sub directory of the period of interest.

# Example data

> /!\ This has nothing to do with effective team schedule in this period

Example data are provided:
- configuration file
- dummy schedule data for 01/01/2018 - 14/01/2018 period (to use it, remove `.example` from directory name)
- results of analyses from dummy data

# About

Program writen in `R` language using libraries:
- `rmarkdown`, `knitr` and `pander` to render HTML output
- `openxlsx` to create Excel output
- and more: `readODS`, `readr`, `lubridate`, `tools`
