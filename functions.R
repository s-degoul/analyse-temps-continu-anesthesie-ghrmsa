

library("readODS")
library("readr")
library("lubridate")

## Extract names from a string, separated by "sep" character. Remove comments surrounded with parenthesis
## Return a character vector
extract.names <- function(x, sep){
    if (length(x) != 1){
        stop("extract.names(): incorrect length of input (must be 1 string)")
    }
    if (is.na(x) | trimws(x) == ""){
        return(NA)
    }
    result <- strsplit(x, split = sep)[[1]]
    for (i in seq(length(result))){
        result[i] <- trimws(gsub("\\([^\\)]*\\)", "", result[i]))
    }
    return(result)
}

## Extract information from schedule file name
extract.schedule.file.info <- function(fn){
    rex <- "^planning_service_([0-9]+)_sem_([0-9]{4}-[0-9]{2}-[0-9]{2})_([0-9]{4}-[0-9]{2}-[0-9]{2})-v([0-9]+).*"
    if (! grepl(rex, fn)){
        stop(paste0("extract.schedule.file.info(): filename ", fn, " badly formated"))
    }

    ward.nb <- gsub(rex, "\\1", fn)
    if (! ward.nb %in% names(wards.conf)){
        stop(paste0("extract.schedule.file.info(): unknow ward for file ", fn))
    }

    return(list(ward = wards.conf[ward.nb],
                start.date = as.Date(gsub(rex, "\\2", fn),
                                     format = "%Y-%m-%d"),
                end.date = as.Date(gsub(rex, "\\3", fn),
                                   format = "%Y-%m-%d"),
                version = gsub(rex, "\\4", fn)))
}

## Compute posts for each physician and date
## Return a dataframe with variables :
## - date,
## - physician,
## - ward,
## - post
compute.posts <- function(dfr, ward){
    result <- data.frame()

    dfr <- as.matrix(dfr)
    row.names(dfr) <- dfr[,1]
    colnames(dfr) <- dfr[1,]
    dfr <- dfr[-1,-1]                     # row names = posts, col names = dates

    ## For each date
    for (j in seq(ncol(dfr))){
        ## POSIX date
        d <- as.POSIXct(colnames(dfr)[j], format = date.format)

        ## For each post
        for (post in row.names(dfr)){
            physicians <- extract.names(dfr[post, j], sep = csv.name.sep)
            for (physician in physicians){
                if (! is.na(physician)){
                    result <- rbind(result,
                                    data.frame(date = d,
                                               physician = physician,
                                               ward = ward,
                                               post = post))
                }
            }
        }
    }

    return(result)
}

## Load adjustments data from adjustment.file
load.adjustements <- function(){
    dfr <- read_ods(adjustment.file,
                    col_types = cols(physician = "f",
                                     date = "c",
                                     start.time = "c",
                                     end.time = "c",
                                     negative = "i",
                                     job.type = "c",
                                     job = "f",
                                     detail = "c"))
    dfr$comments <- NULL
    dfr$date <- as.POSIXct(dfr$date, format = date.format)

    ## Check that all times (date, start and end time) and physicians are not NA
    if (sum(is.na(dfr$physician)) > 0
        | sum(is.na(dfr$date)) > 0
        | sum(is.na(dfr$start.time)) > 0
        | sum(is.na(dfr$end.time)) > 0
        | sum(is.na(dfr$job)) > 0){
        stop("load.adjustements(): missing values for physician, job, date or start / end times")
    }

    ## Check that all dates belongs to period of interest
    idx.dates.before.start <- which(dfr$date < start.date)
    if (length(idx.dates.before.start) > 0){
        stop(paste0("load.adjustements(): some dates are earlier than start date: ",
                    paste(format(dfr$date[idx.dates.before.start], "%d/%m/%Y"), collapse= ", ")))
    }
    idx.dates.after.end <- which(dfr$date > end.date)
    if (length(idx.dates.after.end) > 0){
        warning(paste0("load.adjustements(): some dates are later than end date: ",
                       paste(format(dfr$date[idx.dates.after.end], "%d/%m/%Y"), collapse= ", "),
                       " -> data deleted"))
        dfr <- dfr[- idx.dates.after.end,]
    }

    ## Check if job type is known in configuration, or, if it's NA, that job is already defined in configuration
    for (jt in unique(dfr$job.type)){
        if (! jt %in% unique(jobs.conf.dfr$job.type)){
            if (is.na(jt)){
                idx.jt <- which(is.na(dfr$job.type))
            } else {
                idx.jt <- which(dfr$job.type == jt)
            }
            for (job in unique(dfr[idx.jt,"job"])){
                if (! job %in% unique(jobs.conf.dfr$job)){
                    stop(paste0("load.adjustements(): unknown job type ", jt,
                                " and job ", job))
                }
            }
        }
    }

    ## Complete missing value of job type
    for (i in which(is.na(dfr$job.type))){
        dfr[i, "job.type"] <- jobs.conf[[as.character(dfr[i, "job"])]][["job.type"]]
    }

    ## Warn if unknown physician
    for (p in unique(dfr$physician)){
        if (! p %in% physicians.conf.dfr$physician){
            warning(paste0("Adjustment data: unknown physician ", p))
            dfr <- dfr[which(dfr$physician != p),]
        }
    }
    dfr$physician <- factor(dfr$physician)

    ## Format time in POSIXct
    for (t in c("start", "end")){
        v <- paste0(t, ".time")
        dfr[,v] <- paste(as.character(dfr$date), dfr[,v])
        dfr[,v] <- as.POSIXct(dfr[,v], format = paste("%Y-%m-%d", hour.format))
    }
    ## If end time before start time, add 1 day
    dfr$end.before.start <- dfr$end.time <= dfr$start.time
    dfr[which(dfr$end.before.start), "end.time"] <- dfr[which(dfr$end.before.start), "end.time"] + days(1)
    dfr$end.before.start <- NULL
    ## Compute duration
    dfr$duration <- as.numeric(difftime(dfr$end.time,
                                        dfr$start.time,
                                        units = "hours"))
    dfr[which(dfr$negative == 1), "duration"] <- dfr[which(dfr$negative == 1), "duration"] * -1
    dfr$negative <- NULL
    
    return(dfr)
}


## Get job of a given post, based on jobs config.
## Return string.
## get.job.type <- function(post){
##     for (jt in names(job.conf)){
##         if (post %in% job.conf[[jt]][["posts"]]){
##             return(jt)
##         }
##     }
##     stop(paste0("get.job.type(): unknown post \"", post, "\""))
## }

## Get number of occurences of duplicated dataframe rows
get.nb.occurences.dfr <- function(dfr){
    dfr$line.value <- apply(dfr, 1, function(x){paste(x, collapse = "")})
    dfr$id <- seq(nrow(dfr))
    unique.line.values <- as.data.frame(table(dfr$line.value))
    names(unique.line.values) <- c("line.value", "nb.occ")
    dfr <- merge(dfr,
                 unique.line.values,
                 by = "line.value",
                 all = TRUE)
    return(dfr$nb.occ[order(dfr$id)])
}

## Get type of day of a date: working day (jo) or week-end/holidays (wejf)
get.day.type <- function(d){
    if (class(d)[1] != "POSIXct"){
        stop("get.day.type(): input is not a POSIXct date")
    }
    weekday <- format(d, "%u")
    if (weekday %in% c(6,7) | d %in% holidays){
        return("wejf")
    }
    return("jo")
}

## Compute the total duration (in hours) of given periods (dataframe with start.time and end.time variables)
## FIXME: manage non adjacent or overlapping periods
compute.duration.periods <- function(dfr){
    if (! "start.time" %in% names(dfr) | ! "end.time" %in% names(dfr)){
        stop("compute.duration.periods(): bad names of input columns")
    }
    if (sum(! is.na(dfr$end.time)) == 0 | sum(! is.na(dfr$start.time)) == 0){
        return(NA)
    }
    end.time <- max(dfr$end.time, na.rm = TRUE)
    start.time <- min(dfr$start.time, na.rm = TRUE)
    return(difftime(end.time,
                    start.time,
                    units = "hours"))
}

## Concatenate given jobs, and check that "alone" job are actually alone
concat.jobs <- function(jobs, info = list()){
    if (sum(! is.na(jobs)) == 0){
        return(NA)
    }
    jobs <- unique(jobs[which(! is.na(jobs))])
    jobs.auto <- jobs[which(jobs %in% names(jobs.conf))]
    if (length(jobs.auto) > 1){
        for (job in jobs.auto){
            if (jobs.conf[[job]][["alone"]]){
                context <- ""
                if (length(info) > 0){
                    context <- paste0(" (date = ", info[["date"]], ", ",
                                      "physician = ", info[["physician"]], ")")
                }
                stop(paste0("concat.jobs(): job \"",
                            job,
                            "\" not alone in list \"",
                            paste(jobs.auto, collapse = ", "),
                            "\"",
                            context))
            }
        }
    }
    return(paste(jobs, collapse = concat.info.sep))
}

## Display boolean in french
display.boolean <- function(x){
    return(ifelse(x, "oui", "non"))
}


## Compute posts from GTMed data
compute.GTMed.posts <- function(dfr){
    ## Sanitizing dataframe
    ## Removing first 5 rows
    dfr <- dfr[-seq(5),]
    ## Removing colum 2 (type of physician)
    dfr <- dfr[,-2]

    col.nb <- ncol(dfr)

    result <- data.frame()
    for (i in seq(nrow(dfr))){
        line <- as.character(dfr[i,])
        if (grepl("^Semaine", line[1])){
            dates <- numeric(0)
            j <- 2
            while((is.na(line[j])
                  | grepl("[a-zA-Z]{2} [0-9]{2}\\/[0-9]{2}\\/[0-9]{4}", line[j]))
                  & j <= col.nb){
                      if (is.na(line[j])){
                          if (length(dates) < (j-2)){
                              stop(paste0("Line ", i, " column ", j, ": no date already defined"))
                          }
                          dates <- c(dates,
                                     dates[length(dates)])
                      }
                      else {
                          dates <- c(dates,
                                     as.POSIXct(strsplit(line[j], " ")[[1]][2],
                                                format = "%d/%m/%Y"))
                      }
                      j <- j + 1
                  }
            ## dates <- as.POSIXct(dates, origin = "1970-01-01")
        }
        else if (!is.na(line[1])){
            if (length(dates) == 0){
                stop(paste0("Line ", i, ": no date already defined"))
            }
            p <- trimws(line[1])
            for (d in unique(dates)){
                posts <- line[which(dates == d) + 1]
                is.na(posts[which(posts == "RS")]) <- TRUE
                posts[which(posts %in% c("CA", "RTT"))] <- "CA/RTT"
                if (sum(!is.na(posts)) > 0){
                    result <- rbind(result,
                                    data.frame(date = as.POSIXct(d, origin = "1970-01-01"),
                                               physician = p,
                                               posts = paste(trimws(posts[which(!is.na(posts))]),
                                                             collapse = " ")))
                }
            }
        }
    }

    return(result)
}



## Get theoretical working time for a physician at given date
get.phys.theo.wt <- function(x){
    if (length(x) == 2){
        physician <- x[1]
        date <- x[2]
    }
    else {
        stop("get.phys.theo.wt(): wrong length value for input data (must be 2: physician and date)")
    }
    if (is.na(date) | ! grepl("^[0-9]{4}-[0-9]{2}-[0-9]{2}$", date)){
        stop(paste0("get.phys.theo.wt(): wrong format for date \"", date, "\""))
    }

    date <- as.POSIXct(date)
    
    if (is.na(physician) | ! physician %in% names(physicians.conf)){
        return(NA)
    }
    ## Handling wt exceptions
    if (! is.null(physicians.conf[[physician]]$wt.exceptions)){
        for (i in seq(length(physicians.conf[[physician]]$wt.exceptions))){
            if (date >= physicians.conf[[physician]]$wt.exceptions[[i]]$start.date &
                date <= physicians.conf[[physician]]$wt.exceptions[[i]]$end.date){
                if (is.na(physicians.conf[[physician]]$wt.exceptions[[i]]$contract)){
                    return(NA)
                }
                target.work.duration.day <- contracts[[physicians.conf[[physician]]$wt.exceptions[[i]]$contract]]$target.work.duration.day
                if (get.day.type(date) == "wejf"){
                    ## ## NA (and not 0) if wtp is NA
                    ## return(physicians.conf[[physician]]$wt.exceptions[[i]]$wtp * 0)
                    return(0)
                }
                if (is.null(physicians.conf[[physician]]$wt.exceptions[[i]]$wd)){
                    return(physicians.conf[[physician]]$wt.exceptions[[i]]$wtp * target.work.duration.day)
                }
                if (! format(date, "%u") %in% physicians.conf[[physician]]$wt.exceptions[[i]]$wd){
                    return(0)
                }
                return(target.work.duration.day)
            }
        }
    }
    ## If exceptions not encountered
    if (is.na(physicians.conf[[physician]]$contract)){
        return(NA)
    }
    target.work.duration.day <- contracts[[physicians.conf[[physician]]$contract]]$target.work.duration.day
    if (get.day.type(date) == "wejf"){
        ## ## NA (and not 0) if wtp is NA
        ## return(physicians.conf[[physician]]$wtp * 0)
        return(0)
    }
    if (is.null(physicians.conf[[physician]]$wd)){
        return(physicians.conf[[physician]]$wtp * target.work.duration.day)
    }
    if (! format(date, "%u") %in% physicians.conf[[physician]]$wd){
        return(0)
    }
    return(target.work.duration.day)
}


## Get physician working time proportion for given date
## get.phys.wtp <- function(x){
##     if (length(x) == 2){
##         physician <- x[1]
##         date <- x[2]
##     }
##     else {
##         stop("get.phys.wtp: wrong length value for input data (must be 2: physician and date)")
##     }
    
##     if (is.na(physician) | ! physician %in% names(physicians.conf)){
##         return(NA)
##     }
##     if (is.null(physicians.conf[[physician]]$wt.exceptions) | is.na(date)){
##         return(physicians.conf[[physician]]$wtp)
##     }
##     if (grepl("^[0-9]{4}-[0-9]{2}-[0-9]{2}$", date)){
##         date <- as.POSIXct(date)
##         for (i in seq(length(physicians.conf[[physician]]$wt.exceptions))){
##             if (date >= physicians.conf[[physician]]$wt.exceptions[[i]]$start.date &
##                 date <= physicians.conf[[physician]]$wt.exceptions[[i]]$end.date){
##                 return(physicians.conf[[physician]]$wt.exceptions[[i]]$wtp)
##             }
##         }
##     } else {
##         if (! grepl("^[0-9]{4}_[0-9]{2}$", date)){
##             stop("get.phys.wtp: wrong format for week (must be YYYY_WW)")
##         }
##         week.num <- as.numeric(gsub("_", ".", date))

##         ## To collect wtp for the 5 working days of the week
##         days.wtp <- numeric(0)

##         ## FIXME: start date and end date of wtp exception in the same week is not supported
##         for (i in seq(length(physicians.conf[[physician]]$wt.exceptions))){
##             start.week.num <- as.numeric(format(physicians.conf[[physician]]$wt.exceptions[[i]]$start.date,
##                                                 format = "%G.%V"))
##             end.week.num <- as.numeric(format(physicians.conf[[physician]]$wt.exceptions[[i]]$end.date,
##                                               format = "%G.%V"))
##             if (week.num > start.week.num & week.num < end.week.num){
##                 ## return(physicians.conf[[physician]]$wt.exceptions[[i]]$wtp)
##                 days.wtp <- rep(physicians.conf[[physician]]$wt.exceptions[[i]]$wtp, 5)
##             } else if (week.num == start.week.num){
##                 days.wtp <- c(days.wtp,
##                               rep(physicians.conf[[physician]]$wt.exceptions[[i]]$wtp,
##                               (6 - min(as.numeric(format(physicians.conf[[physician]]$wt.exceptions[[i]]$start.date, "%u")),
##                                        6)))) # days taken into account until saturday, and including start date (5 + 1)
##             } else if (week.num == end.week.num){
##                 days.wtp <- c(days.wtp,
##                               rep(physicians.conf[[physician]]$wt.exceptions[[i]]$wtp,
##                                   min(as.numeric(format(physicians.conf[[physician]]$wt.exceptions[[i]]$end.date, "%u")),
##                                       5))) # days taken into account until saturday, and including end date
##             }
##         }
##         if (length(days.wtp) > 0){
##             if (length(days.wtp) < 5){
##                 days.wtp <- c(days.wtp,
##                               rep(physicians.conf[[physician]]$wtp,
##                                   5 - length(days.wtp)))
##             } else if (length(days.wtp) > 5){
##                 stop(paste0("get.phys.wtp: wrong length of computed days.wtp: ",
##                             physician, "; ",
##                             week.num, "; ",
##                             paste(days.wtp, collapse = ", ")))
##             }
##             return(ifelse(sum(!is.na(days.wtp) > 0),
##                           sum(days.wtp, na.rm = TRUE)/length(days.wtp),
##                           NA))
##         }
##     }
##     return(physicians.conf[[physician]]$wtp)
## }


## /!\ FIXME: 20/08/21 en attente des retours de la direction pour la gestion du temps non clinique
## Get minimal value of non clinic work time given configured exceptions
get.min.nc.work.duration.week <- function(week){
    if (! grepl("^[0-9]{4}_[0-9]{2}$", week)){
        stop("get.min.nc.work.duration.week(): wrong format for week (must be YYYY_WW)")
    }
    if (length(min.nc.work.duration.week.exc) > 0){
        week.num <- as.numeric(gsub("_", ".", week))
        for (i in seq(length(min.nc.work.duration.week.exc))){
            start.week.num <- as.numeric(format(min.nc.work.duration.week.exc[[i]]$start.date,
                                                format = "%G.%V"))
            end.week.num <- as.numeric(format(min.nc.work.duration.week.exc[[i]]$end.date,
                                              format = "%G.%V"))
            if (week.num >= start.week.num & week.num <= end.week.num){
                return(min.nc.work.duration.week.exc[[i]]$wd)
            }
        }
    }
    return(min.nc.work.duration.week)
}

## Get week day from day's number
get.week.day.from.nb <- function(day.nbs){
    result <- character(0)
    for (d in day.nbs){
        d <- as.integer(d)
        if (is.na(d) | d < 1 | d > 7){
            stop(paste0("get.week.day.from.nb: illegal value of day n°: ", d))
        }
        result <- c(result, names(weekday.names[which(weekday.names == d)]))
    }
    return(result)
}
